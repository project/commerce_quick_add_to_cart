<?php

namespace Drupal\commerce_quick_add_to_cart\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class QuickAddToCartAutocompleteController extends ControllerBase {

  /**
   * Menu callback for Quick Add To Cart autocompletion.
   *
   * Like other autocomplete functions, this function inspects the 'q' query
   * parameter for the string to use to search for suggestions.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions for Views tags.
   */
  public function autocomplete($bundle, $field, Request $request) {
    $matches = [];
    $string = $request->query->get('q');
    // Get matches from default views.
    $query = $this->entityTypeManager()->getStorage('commerce_product')->getQuery();
    $query->condition('type', $bundle);
    $query->condition($field, '%' . $string . '%', 'LIKE');
    $ids = $query->execute();
    $products = $this->entityTypeManager()->getStorage('commerce_product')->loadMultiple($ids);

    foreach ($products as $product) {
      $matches[] = ['value' => $product->id(), 'label' => Html::escape($product->label())];
    }
    return new JsonResponse($matches);
  }

}
